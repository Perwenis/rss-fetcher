Instalacja już po ściagnięciu paczki composerem (zakładam że Laravel i jego konfiguracja z baza itd. jest zrobiona):
- Dodać do listy providerów (config/app.php):

> Rss\Provider\RssServiceProvider::class

- Dodać tabele: 

> php artisan migrate

- Następnie assets:

> php artisan vendor:publish

dodać je do naszej kompilacji js, np.
> require('../../vendor/rss/js/app');

przekompilować assets:
> npm run dev

<br/>
<b>Znane mi problemy:</b><br/>
- Jeśli podczas migracji jest błąd 'max key length is 767 bytes' trzeba w jakims providerze dodać:<br/>

> Schema::defaultStringLength(191);

- Główna ścieżka jest na '/', więc możliwe że trzeba będzie zakomentować inne routes