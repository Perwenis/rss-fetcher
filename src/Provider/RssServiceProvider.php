<?php

namespace Rss\Provider;

use Rss\Controller\RssEntryController;
use Rss\Controller\RssFeedController;
use Rss\Repository\RssEntryRepository;
use Rss\Repository\RssEntryRepositoryInterface;
use Rss\Repository\RssSourceRepository;
use Rss\Repository\RssSourceRepositoryInterface;
use Rss\RssFetcher\EntityFactory\RssEntryFactory;
use Rss\RssFetcher\EntityFactory\RssEntryFactoryInterface;
use Rss\RssFetcher\Parser\ParserRetriever;
use Rss\RssFetcher\Parser\ParserRetrieverInterface;
use Rss\RssFetcher\Parser\ParserTags;
use Rss\RssFetcher\Parser\AtomParser;
use Rss\RssFetcher\RssFetcher;
use Rss\RssFetcher\RssFetcherInterface;
use Illuminate\Support\ServiceProvider;

class RssServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes.php');
        $this->loadViewsFrom(__DIR__.'/../View', 'Rss');
        $this->loadMigrationsFrom(__DIR__.'/../Migration');
        $this->publishes([
            __DIR__.'/../Asset' => resource_path('vendor/rss'),
        ], 'public');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(RssFetcherInterface::class, function ($app) {
            return new RssFetcher($app->make(ParserRetrieverInterface::class));
        });
        $this->app->singleton(ParserRetrieverInterface::class, function ($app) {
            return new ParserRetriever([
                ParserTags::XML_FETCHER_TAG => new AtomParser($app->make(RssEntryFactoryInterface::class)),
            ]);
        });
        $this->app->singleton(RssEntryFactoryInterface::class, function () {
            return new RssEntryFactory();
        });

        $this->app->singleton(RssSourceRepositoryInterface::class, RssSourceRepository::class);
        $this->app->singleton(RssEntryRepositoryInterface::class, RssEntryRepository::class);
    }
}
