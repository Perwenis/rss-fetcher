<?php

namespace Rss\RssFetcher;

use Rss\RssFetcher\Entity\RssEntryInterface;

interface RssFetcherInterface
{
    /**
     * @return RssEntryInterface[]
     */
    public function fetch(string $url): array;
}