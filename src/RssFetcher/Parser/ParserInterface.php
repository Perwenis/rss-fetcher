<?php

namespace Rss\RssFetcher\Parser;

use Rss\RssFetcher\Entity\RssEntryInterface;

interface ParserInterface
{
    /**
     * @return RssEntryInterface[]
     */
    public function parse(string $content): array;
}