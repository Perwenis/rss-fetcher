<?php

namespace Rss\RssFetcher\Parser;

class AtomParser extends AbstractParser
{
    public function parse(string $content): array
    {
        $rssEntries = [];
        $parsedContent = new \SimpleXMLElement($content);
        foreach ($parsedContent->entry as $entry) {
            $createdAt = new \DateTime();
            $createdAt->setTimestamp(strtotime((string)$entry->published));
            $updatedAt = new \DateTime();
            $updatedAt->setTimestamp(strtotime((string)$entry->updated));
            $rssEntries[] = $this->rssEntryFactory->createFromArray([
                'id' => (string)$entry->id,
                'title' => (string)$entry->title,
                'createdAt' => $createdAt,
                'updatedAt' => $updatedAt,
                'content' => (string)$entry->content->asXml(),
            ]);
        }
        return $rssEntries;
    }
}