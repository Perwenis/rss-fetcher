<?php

namespace Rss\RssFetcher\Exception;

class UnsupportedContentFormatException extends \Exception implements RssFetcherExceptionInterface
{
    public function __construct(string $contentType)
    {
        parent::__construct('Nie obsługiwany format danych: ' . $contentType);
    }
}