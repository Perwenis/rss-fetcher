@extends('Rss::layout')
@section('content')
    <div id="rss-form-container">
        <form method="post" id="rss-form" action="{{ action('\Rss\Controller\RssFeedController@getFeedAction') }}">
            <div class="row">
                <div class="col-md-11">
                    <div class="form-group">
                        <input type="text" placeholder="url" class="form-control" id="input-url" name="url" value="{{ $url }}">
                        <p class="url-error bg-danger"></p>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <button type="submit" class="btn btn-default">Pokaż</button>
                    </div>
                </div>
            </div>
        </form>
        <div id="rss-list">

        </div>
    </div>
@endsection