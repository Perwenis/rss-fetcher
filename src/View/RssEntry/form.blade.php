@extends('Rss::layout')
@section('content')
    <div id="rss-form-container">
        <form method="post" id="rss-entry-form" action="{{ action('\Rss\Controller\RssEntryController@saveAction', ['id' => $rssEntry->id]) }}">
            <input type="hidden" value="{{ $rssEntry->rss_source_id }}" name="rss_source_id">
            <div class="form-group">
                <label for="external_id">id:</label>
                <input type="text" class="form-control" id="external_id" name="external_id" value="{{ $rssEntry->external_id }}">
            </div>
            <div class="form-group">
                <label for="title">Tytuł:</label>
                <input type="text" class="form-control" id="title" name="title" value="{{ $rssEntry->title }}">
            </div>
            <div class="form-group">
                <label for="content">Treść:</label>
                <input type="text" class="form-control" id="content" name="content" value="{{ $rssEntry->content }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default">Zapisz</button>
            </div>
        </form>
    </div>
@endsection