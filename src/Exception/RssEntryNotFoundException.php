<?php

namespace Rss\Exception;

class RssEntryNotFoundException extends \Exception implements RssExceptionInterface
{
    public function __construct($id)
    {
        parent::__construct('Nie znaleziono RssEntry o id: ' . $id);
    }
}